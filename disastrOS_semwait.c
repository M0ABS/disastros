#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

/*
 * Author: Magliocca Francesco
 * 
 * */
 
void internal_semWait(){
  /*
		
		Funzione che decrementa il valore del semaforo.
		Se il valore del semaforo è 0 o negativo il chiamante viene messo in attesa.

  */

	// Descrittore del semaforo
	int sem_fd = running->syscall_args[0];
 	SemDescriptor* sem_desc = SemDescriptorList_byFd(&(running->sem_descriptors), sem_fd);

 	if(!sem_desc){
    	running->syscall_retvalue = DSOS_ESEMAPHOREWAIT;
    	return;
  	}

	Semaphore* sem = sem_desc->semaphore;

	sem->count-=1;

 	if(sem->count < 0){
 		SemDescriptorPtr* sem_desc_ptr = SemDescriptorPtr_alloc(sem_desc);
	    List_insert(&(sem->waiting_descriptors), sem->waiting_descriptors.last, (ListItem*)sem_desc_ptr);
	    running->status = Waiting;
	    List_insert(&waiting_list, waiting_list.last, (ListItem*)running);
 	}
 	running->syscall_retvalue=0;
}
