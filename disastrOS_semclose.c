#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

/*
 * Author: Capozzi Leonardo
 *
 * */

void internal_semClose(){

  

    /*
    running è una struttura di tipo PCB, che rappresenta un processo in stato di running, 
    all'interno del PCB sono salvate varie informazioni riguarti il processo in esecuzione 
    come ad esempio il pid cioè il numero identificativo del processo. 
    Tra queste informazioni ce ne sono alcune riguardati le chiamate a sistema ovvero le syscall:
		int syscall_num;
  		long int syscall_args[DSOS_MAX_SYSCALLS_ARGS];
  		int syscall_retvalue;
    */

    //acquisizione file descriptor del semaforo dal processo corrente

    int fd = running -> syscall_args[0];    

    /*
     * SemDescriptor*  SemDescriptorList_byFd(ListHead* l, int fd);
     *
     * SemDescriptor*  SemDescriptorList_byFd(ListHead* l, int fd){
     *    ListItem* aux=l->first;
     *    while(aux){
     *      SemDescriptor* d=(SemDescriptor*)aux;
     *      if (d->fd==fd)
     *        return d;
     *      aux=aux->next;
     *    }
     *    return 0;
     *  }
     */

    /*
     * All'interno di running, prendo sem_descriptors cioè una linkedlist contenente i descrittori dei semafori, che insieme al numero identificativo
     * del semaforo ottenuto precedentemente, li passo alla funzione SemDescriptorList_byFd(ListHead* l,int fd) contenuta nel file "disastrOS_semdescriptor.c"
     * tale funzione ritorna il descrittore del semaforo se e solo se all'interno della lista sem_descriptors viene trovato un numero identificativo
     * uguale a quello passato come parametro, in caso ciò accada, viene restituito il SemDescriptor del semaforo da chiudere, in caso contrario viene restituito
     * 0 e quindi non c'è nessun semaforo da chiudere.
     *
     */

    SemDescriptor* sem_desc = SemDescriptorList_byFd(&running->sem_descriptors,fd);

    if( sem_desc == 0 ){
        printf("Semaforo inesistente\n");
        running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
        return;
    }

    //il semaforo viene rimosso dalla lista dei processi
    sem_desc = (SemDescriptor*) List_detach(&running->sem_descriptors, &(sem_desc->list));
    if( !sem_desc ){
        printf("Rimozione del descrittore del semaforo dalla lista dei processi non riuscita");
        running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
        return;
    } 

    //Acquisizione del semaforo dal descrittore

    Semaphore* sem=sem_desc->semaphore;

    //Rimozione del puntatore del descrittore dalla lista delle risorse

    SemDescriptorPtr* sem_desc_ptr = (SemDescriptorPtr*) List_detach(&sem->descriptors,(ListItem*)sem_desc->ptr);
    
    if( !sem_desc_ptr ){
        printf("Rimozione del puntatore del descrittore dalla lista delle risorse non riuscita");
        running->syscall_retvalue = DSOS_ESEMAPHORECLOSE;
        return;
    } 

    if( sem->descriptors.size == 0 ){
        printf("Rimozione semaforo %d\n", sem->id);
        List_detach(&semaphores_list, (ListItem*)sem);
    }

    SemDescriptor_free(sem_desc);
    SemDescriptorPtr_free(sem_desc_ptr);
    running->syscall_retvalue=0;

}
