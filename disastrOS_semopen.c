#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS_globals.h"
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
#include "disastrOS_constants.h"

/*
	Author:Simone Marino
*/
void internal_semOpen(){
// preleviamo le info del semaforo dal pcb
  int sem_id = running->syscall_args[0];
  int oflag  = running->syscall_args[1];
  int count  = running->syscall_args[2];
  printf("[*] PID %d requested open of semaphore %d\n", running->pid, sem_id);

  // controlliamo se il semaforo con quell'ID è gia stato aperto
  Semaphore* sem = SemaphoreList_byId(&semaphores_list, sem_id);
  if (oflag & DSOS_CREATE) {
    if (sem) {
      running->syscall_retvalue = DSOS_ESEMAPHORECREATE;
      return;
    }
    printf("[+] Semaphore with id %d doesn't exist, allocating it\n", sem_id);
    sem = Semaphore_alloc(sem_id, count);
    // aggiungiamo il semaforo creato nella lista
    List_insert(&semaphores_list, semaphores_list.last, &(sem->list));
  }
    //controlliamo se è tutto ok, adesso dovremmo avere il semaforo ,errori a parte

  if (!sem) {
    running->syscall_retvalue = DSOS_ESEMAPHOREOPEN;
    return;
  }
  //se viene richiesto di aprire un semaforo mentre un processo lo sta già utilizzando ritorna errore
    if ((oflag & DSOS_EXCL) && sem->descriptors.size) {
    running->syscall_retvalue = DSOS_ESEMAPHORENOEXCL;
    return;

    }
// creiamo il descrittore per la risorsa, e aggiungiamolo in nella lista
  SemDescriptor*desc= SemDescriptor_alloc(running->last_sem_fd,sem, running);
    if(!desc){
        running->syscall_retvalue= DSOS_ERESOURCENOFD;
        return;
    }
    running->last_sem_fd++; //increment last sem_fd number
    List_insert(&running->sem_descriptors, running-> sem_descriptors.last,(ListItem*)desc);

      // aggiungiamolo alla lista

      SemDescriptorPtr* desc_ptr= SemDescriptorPtr_alloc(desc);
      desc->ptr= desc_ptr;
      List_insert(&sem->descriptors, sem->descriptors.last,(ListItem*)desc_ptr);

      //return descriptor to the process
        // printf("[*] fine della semOpen con id %d per il processo %d e flags %d\n", sem_id, running->pid, oflag);

  // SemaphoreList_print(&semaphores_list);


     running->syscall_retvalue= desc->fd;


}
