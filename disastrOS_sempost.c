#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
#include "disastrOS_globals.h"


/*
 * Author: Ottobre Ludovico
 * 
 * */



void internal_semPost(){
  
  // (1) Retrieving file descriptor of the semaphore
  int fd = running -> syscall_args[0];
  
  // (2) Retrieving semaphore descriptor from fd
  SemDescriptor* sem_desc = SemDescriptorList_byFd(&running -> sem_descriptors, fd);
  
  
  // (3)If that descriptor does not belong to te process, return an error
  if(!sem_desc){
	  running -> syscall_retvalue = DSOS_ERESOURCEOPEN;
	  return;
  }
  
  
  printf("Pid %d requesting sempost on %d semaphore\n", running->pid, sem_desc->semaphore->id);
  
  // (4)If the semaphore is at 0, retrieve first thread to be resumed
  Semaphore* semaphore = sem_desc -> semaphore;

  // (5)Incresing semaphore value
  semaphore->count++;

  if(semaphore->count <= 0){ 
    SemDescriptorPtr* sem_desc_ptr = (SemDescriptorPtr*)List_detach(&semaphore->waiting_descriptors, semaphore->waiting_descriptors.first);

    if(!sem_desc_ptr){
        running->syscall_retvalue = DSOS_ERESOURCEOPEN;
          return;
    }
    
    SemDescriptor* sem_desc = sem_desc_ptr->descriptor;
    PCB* ready_process = sem_desc->pcb; 
   
    
    PCB* ret = (PCB*)List_detach(&waiting_list, (ListItem*)ready_process );
    
    if(!ret){
      printf("[!] Process with Pid %d is not in waiting list\n", ready_process->pid);
      running->syscall_retvalue = DSOS_ERESOURCEOPEN;
      return;
    }
    
    // (7)Marking it ad 'Ready'
    ready_process -> status = Ready;
    
    // (6)Inserting our thread to the Ready list
    List_insert(&ready_list, ready_list.last, (ListItem*) ready_process);
    
  }
  running -> syscall_retvalue = 0;
  
}
